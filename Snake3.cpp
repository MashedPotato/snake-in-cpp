// Compiler string
//g++ Snake3.cpp -lglut -lGL -lGLEW -lGLU -o Snake3

// Include OpenGL libraries
#include <GL/freeglut.h>
#include <GL/gl.h>

// Include standard c++ libraries for calculation and output
#include <iostream>
#include <cstdlib>

//////////////// VARIABLES ///////////////////

// Max value of choordinate system
int widthX = 21;
int heightY = 21;

// Game state
bool gameOver = false;
// Frames per second
int FPS = 8;

// Player konditions
int snakeLength = 4;
int score = 0;
// The Snake list is 400 values long, but 396 values are not defined-
// untill snakeLength increases
int snakeX[400] = {11, 12, 12, 12};
int snakeY[400] = {11, 11, 12, 13};
// Direction snake head is moving
int direction = 3;

// Set initial choordinates for food
int foodX = 3;
int foodY = 10;

//////////////// FUNCTIONS //////////////////

void Animation()    {
    // Define direction as a switch
     switch(direction)   {
        // Depending on direction's value change snake head x, y values
        case 1:
            snakeX[0] += 1.0;
        break;
        
        case 2:
            snakeY[0] += 1.0;
        break;
        
        case 3:
            snakeX[0] -= 1.0;
        break;

        case 4:
            snakeY[0] -= 1.0;
        break;  
    }
    // Draw player head
    // Head size
    glPointSize(24.0);
    // Start drawing square
    glBegin(GL_POINTS);
        // Head color
        glColor3f(0.1, 0.2, 0.1);
        // Head x, y choordinates
        glVertex2f(snakeX[0], snakeY[0]);
    // Finish drawing
    glEnd();
}

void Draw()   {
    // Draw background
    // Start drawing background square
    glBegin(GL_POLYGON);
        // Set polygon color
        glColor3f(0.4, 0.6, 0.4);
        // Set corner choordinates
        glVertex2f(widthX-0.5, heightY-0.5);
        glVertex2f(0.5, heightY-0.5);
        glVertex2f(0.5, 0.5);
        glVertex2f(widthX-0.5, 0.5);
    // Finish drawing
    glEnd();


    // Snake values
    for(int i = snakeLength-1; i > 0; i--)   {
        // Check if snake head colides with snake body
        if(snakeX[i] == snakeX[0] && snakeY[i] == snakeY[0])    {
            // if head collides with body game over
            gameOver = true;
        }
        while(foodX == snakeX[i] && foodY == snakeY[i]) {
            foodX = (rand() % (widthX - 1) + 1);
            foodY = (rand() % (heightY - 1) + 1);
        }
        // Set last value in list equal to second to last,
        // second to last equals third to last and so on
        snakeX[i] = snakeX[i-1];
        snakeY[i] = snakeY[i-1];


        // Draw snake body
            // Square size
            glPointSize(20.0);
            // Start drawing square
            glBegin(GL_POINTS);
                // Snake body color
                glColor3f(0.2, 0.4, 0.2);
                // Set one point of body on current x, y of i chordinates
                glVertex2f(snakeX[i], snakeY[i]);
            // Finish drawing point
            glEnd();
    }
    // Call function Animation
    Animation();


    // Draw food
    // square size
    glPointSize(20.0);
            // Start drawing square
        glBegin(GL_POINTS);
            // Square color
            glColor3f(0.2, 0.3, 0.2);
            // food x, y choordinates
            glVertex2f(foodX, foodY);
    // Finish drawing
    glEnd();
}

void Collision()    {
    // If Snake head moves outside box game over
    if(snakeX[0] > (widthX - 1) || snakeX[0] < 1 || 
       snakeY[0] > (heightY - 1) || snakeY[0] < 1)   {
            gameOver = true;
    }

    // If snake head collides with food
    if((snakeX[0] == foodX) && (snakeY[0] == foodY))  {
        // Change x, y chordinates of food to random value 1 - 20       
        foodX = (rand() % (widthX - 1) + 1);
        foodY = (rand() % (heightY - 1) + 1);
        // Increase snake length
        snakeLength ++;
        // Increase score
        score ++;
    }
}

void Display()  {
    // Clear buffer
    glClear(GL_COLOR_BUFFER_BIT);

    // Call function Draw
    Draw();

    // Swap buffers
    glutSwapBuffers();
    // Update all drawings
    glFlush();
}

void Reshape(int w, int h)  {
    // Window width, height (int w, int h) after reshape
    // Define drawing area w and h
    glViewport(0, 0, (GLsizei)w, (GLsizei)h);
    // Set the matrix to choordinates
    glMatrixMode(GL_PROJECTION);
    // No reshape changes will be made in the first frame
    glLoadIdentity();
    // Settup choordinate values, I will start from origin.
    // I want the choordinates to start from 0 and end on 20 on x,y axises
    // I will use a z axis values -1 to 1 for layers
    glOrtho(0.0, widthX, 0.0, heightY, -1.0, 1.0);
    // switch back to model view matrix
    glMatrixMode(GL_MODELVIEW);
}

void Timer(int)    {
    // Stop game if game over
    if(gameOver == false)    { 
    // load redisplay   
        glutPostRedisplay();
        // Call function Collition
        Collision();
        // update self every 60th of a second
    glutTimerFunc(1000/FPS, Timer, 0);
    }
    else    {
            // if game over view score
        std::cout << "you scored " << score << " points!" << std::endl;
    }
}

void Keyboard(int key, int x, int y)    {
    // kollar efter keyboard inputs och ändrar Direction    
    if((key == GLUT_KEY_RIGHT) && (direction != 3))   {            
        direction = 1;
    }
    if((key == GLUT_KEY_UP) && (direction != 4)) {            
        direction = 2;
    }
    if((key == GLUT_KEY_LEFT) && (direction != 1))   {            
        direction = 3;
    }
    if((key == GLUT_KEY_DOWN) && (direction != 2))   {            
        direction = 4;
    }
}

void InitDraw()  {
    // Initall background collor
    glClearColor(0.2, 0.4, 0.2, 1.0);
}


//////////////////// MAIN FUNCTION //////////////////////

int main(int argc, char** argv)  {
// General Initiallization
    glutInit(&argc, argv);
    // Enable RGB colors and Double Buffer
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);

// Window Settings
    // Possition initiall window
    glutInitWindowPosition(10, 10);
    // Initiall window size
    glutInitWindowSize(500, 500);
    // Create window with name
    glutCreateWindow("Snake");

// Special OpenGL functions
    // Display function callback
    glutDisplayFunc(Display);
    // Reshape display after window reshape
    glutReshapeFunc(Reshape);
    glutTimerFunc(0, Timer, 0);
    glutSpecialFunc(Keyboard);

// Other functions
    // Inital Drawing function callback
    InitDraw();
    
    // Main loop
    glutMainLoop();
    return 0;
}
